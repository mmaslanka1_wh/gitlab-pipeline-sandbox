package com.williamhill.test;

public class City {

    private final String postalCode;
    private final String code;

    public City(String postalCode, String code) {
        this.postalCode = postalCode;
        this.code = code;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCode() {
        return code;
    }
}
